stages:
    - no_ci
    - fmt
    - tests
    - quality
    - package
    - publish_crate
    - publish_doc 
    - prerelease
    - release

variables:
  CARGO_HOME: $CI_PROJECT_DIR/cargo

.ci_conditions: &ci_only_conditions
    changes:
      - .gitlab-ci.yml
      - Cargo.toml
      - Cargo.lock
      - bin/**/*
      - lib/**/*
      - release/**/*

.ci_except_conditions: &ci_except_conditions
    variables:
      - $CI_COMMIT_MESSAGE =~ /^wip*/i

.ci_conditions:
  only:
    <<: *ci_only_conditions
  except:
    <<: *ci_except_conditions

block_ci:
  only:
    <<: *ci_except_conditions
  stage: no_ci
  when: manual
  allow_failure: false
  script:
    - exit -1

skip_ci:
  except:
    <<: *ci_only_conditions
  stage: no_ci
  when: manual
  script:
    - echo 1

.rust_stable_lin64:
  extends: .ci_conditions
  image: registry.duniter.org/docker/dunitrust/dunitrust-ci-lin64:latest
  tags:
    - redshift-docker-runner
  before_script:
    - export PATH="$HOME/.cargo/bin:$PATH"
    - rustup show
    - rustc --version && cargo --version

.rust_stable_lin64_arch:
  extends: .ci_conditions
  image: registry.duniter.org/docker/rust/arch-builder:latest
  tags:
    - redshift-docker-runner
  before_script:
    - export PATH="$HOME/.cargo/bin:$PATH"
    - rustup show
    - rustc --version && cargo --version

.rust_stable_armv7:
  extends: .ci_conditions
  image: registry.duniter.org/docker/rust/armv7-builder:latest
  tags:
    - redshift-docker-runner
  before_script:
    - rustc --version && cargo --version

.rust_stable_win64:
  extends: .ci_conditions
  image: registry.duniter.org/docker/rust/win64-builder:v1.37.1
  tags:
    - redshift-docker-runner
  before_script:
    - rustc --version && cargo --version

fmt:
  extends: .rust_stable_lin64
  stage: fmt
  before_script:
    - cargo fmt -- --version
  script:
    - cargo fmt -- --check
    
tests:linux64:stable:
  extends: .rust_stable_lin64
  stage: tests
  tags:
    - redshift-docker-runner
  script: 
    - cd bin/dunitrust-server
    - RUSTFLAGS="-D warnings" cargo build --features=ssl
    - cargo test --all
    - cargo test --all -- --ignored

tests:arm-v7-:stable:
  extends: .rust_stable_armv7
  stage: tests
  allow_failure: true
  when: manual
  except:
    refs:
      - tags
  script: 
    - cd bin/dunitrust-server
    - RUSTFLAGS="-D warnings" cargo build --target=armv7-unknown-linux-gnueabihf --features=ssl
    - cargo test --all --target=armv7-unknown-linux-gnueabihf

tests:arm-v7:stable:
  extends: .rust_stable_armv7
  stage: tests
  only:
    - tags
  script: 
    - cd bin/dunitrust-server
    - RUSTFLAGS="-D warnings" cargo build --target=armv7-unknown-linux-gnueabihf --features=ssl
    - cargo test --all --target=armv7-unknown-linux-gnueabihf

tests:win64:stable:
  extends: .rust_stable_win64
  stage: tests
  script: 
    - cd bin/dunitrust-server
    - RUSTFLAGS="-D warnings" cargo build --target=x86_64-pc-windows-gnu
    - cargo test --package dunitrust --target=x86_64-pc-windows-gnu
    - cargo test --package durs-conf --target=x86_64-pc-windows-gnu
    - cargo test --package durs-core --target=x86_64-pc-windows-gnu
    - cargo test --package durs-message --target=x86_64-pc-windows-gnu
    - cargo test --package durs-module --target=x86_64-pc-windows-gnu
    - cargo test --package durs-network --target=x86_64-pc-windows-gnu
    - cargo test --package durs-bc-db-reader --target=x86_64-pc-windows-gnu
    - cargo test --package durs-bc-db-writer --target=x86_64-pc-windows-gnu
    - cargo test --package durs-blockchain --target=x86_64-pc-windows-gnu
    - cargo test --package durs-dbs-tools --target=x86_64-pc-windows-gnu
    #- cargo test --package durs-skeleton-module --target=x86_64-pc-windows-gnu
    - cargo test --package durs-ws2p-v1-legacy --target=x86_64-pc-windows-gnu
    - cargo test --package durs-ws2p --target=x86_64-pc-windows-gnu
    - cargo test --package durs-ws2p-messages --target=x86_64-pc-windows-gnu
    - cargo test --package dup-crypto --target=x86_64-pc-windows-gnu
    - cargo test --package durs-common-tools --target=x86_64-pc-windows-gnu
    - cargo test --package dubp-user-docs --target=x86_64-pc-windows-gnu
    - cargo test --package json-pest-parser --target=x86_64-pc-windows-gnu
    - cargo test --package durs-network-documents --target=x86_64-pc-windows-gnu
    - cargo test --package rules-engine --target=x86_64-pc-windows-gnu
    - cargo test --package durs-wot --target=x86_64-pc-windows-gnu

clippy:
  extends: .rust_stable_lin64
  before_script:
    - cargo clippy -- -V
  stage: quality
  script:
    - cargo clippy --all -- -D warnings --verbose

audit:manual:
  extends: .rust_stable_lin64
  before_script:
    - cargo install --force cargo-audit
  stage: quality
  script:
    - cargo audit
  when: manual
  except:
    refs:
      - dev

audit:
  extends: .rust_stable_lin64
  before_script:
    - cargo install --force cargo-audit
  stage: quality
  script:
    - cargo audit
  only:
    refs:
      - dev

publish:crate:
  extends: .rust_stable_lin64
  stage: publish_crate
  when: manual
  allow_failure: true
  only:
    - tags
  script:
    - IFS='/' read -r first a <<< "$CI_COMMIT_TAG"
    - cd $first
    - cargo login $DUNITER_CRATES_TOKEN
    - cargo publish

package:test:lin64:deb:
  extends: .rust_stable_lin64
  stage: package
  when: manual
  except:
    refs:
      - tags
  script:
    - bash "release/arch/linux-x64/build-deb.sh" "$(date +%Y%m%d).$(date +%H%M).$(date +%S)"
  artifacts:
    paths:
      - work/bin/
    expire_in: 1 weeks

package:test:lin64:arch:
  extends: .rust_stable_lin64_arch
  stage: package
  when: manual
  except:
    refs:
      - tags
  script:
    - sudo chown -R builduser /builds/nodes/rust/duniter-rs/bin/dunitrust-server/src
    - sudo -u builduser -n bash "release/arch/linux-x64/build-arch.sh" "$(date +%Y%m%d).$(date +%H%M).$(date +%S)"
  artifacts:
    paths:
      - work/bin/
    expire_in: 1 weeks

.docker-build-app-image:
  extends: .ci_conditions
  stage: package
  image: docker:18.06
  tags:
    - redshift-docker-runner
  services:
    - docker:18.06-dind
  variables:
    #DOCKER_TLS_CERTDIR: "/certs"
    #DOCKER_DRIVER: overlay2
    LAST_COMMIT_HASH: $CI_COMMIT_SHORT_SHA
  before_script:
    - docker info
  script:
    - docker pull $CI_REGISTRY_IMAGE:$IMAGE_TAG || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:$IMAGE_TAG --pull -t "$CI_REGISTRY_IMAGE:$IMAGE_TAG" -f release/docker/Dockerfile .
    - docker login -u "gitlab-ci-token" -p "$CI_BUILD_TOKEN" $CI_REGISTRY
    - docker push "$CI_REGISTRY_IMAGE:$IMAGE_TAG"

package:test:docker-test-image:
  extends: .docker-build-app-image
  except:
    refs:
      - dev
      - tags
  when: manual
  variables:
    IMAGE_TAG: "test-image"

package:test:docker:
  extends: .docker-build-app-image
  only:
    refs:
      - dev
  except:
    refs:
      - tags
  variables:
    IMAGE_TAG: "dev"

package:test:armv7:
  extends: .rust_stable_armv7
  stage: package
  when: manual
  except:
    refs:
      - tags
  script:
    - bash "release/arch/armv7/build-armv7.sh" "$(date +%Y%m%d).$(date +%H%M).$(date +%S)"
  artifacts:
    paths:
      - work/bin/
    expire_in: 1 weeks

package:test:win64:
  extends: .rust_stable_win64
  stage: package
  when: manual
  except:
    refs:
      - tags
  script:
    - bash "release/arch/win64/build-win64.sh" "$(date +%Y%m%d).$(date +%H%M).$(date +%S)"
  artifacts:
    paths:
      - work/bin/
    expire_in: 1 weeks

package:prod:linux64:
  extends: .rust_stable_lin64
  stage: package
  only:
    - tags
  script:
    - bash "release/arch/linux-x64/build-lin-x64.sh" "${CI_COMMIT_TAG#v}"
  artifacts:
    paths:
      - work/bin/
    expire_in: 2 weeks

package:prod:lin64:deb:
  extends: .rust_stable_lin64
  stage: package
  when: manual
  only:
    - tags
  script:
    - bash "release/arch/linux-x64/build-deb.sh" "${CI_COMMIT_TAG#v}"
  artifacts:
    paths:
      - work/bin/
    expire_in: 2 weeks

package:prod:lin64:arch:
  extends: .rust_stable_lin64_arch
  stage: package
  when: manual
  only:
    - tags
  script:
    - sudo chown -R builduser /builds/nodes/rust/duniter-rs/bin/dunitrust-server/src
    - sudo -u builduser -n bash "release/arch/linux-x64/build-arch.sh" "${CI_COMMIT_TAG#v}"
  artifacts:
    paths:
      - work/bin/
    expire_in: 2 weeks

package:prod:docker:
  stage: package
  only:
    - tags
  image: docker:18.06
  tags:
    - redshift-docker-runner
  services:
    - docker:18.06-dind
  variables:
    LAST_COMMIT_HASH: $CI_COMMIT_SHORT_SHA
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG" -f release/docker/Dockerfile .
    - docker login -u "gitlab-ci-token" -p "$CI_BUILD_TOKEN" $CI_REGISTRY
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG"

package:prod:armv7:
  extends: .rust_stable_armv7
  stage: package
  only:
    - tags
  script:
    - bash "release/arch/armv7/build-armv7.sh" "${CI_COMMIT_TAG#v}"
  artifacts:
    paths:
      - work/bin/
    expire_in: 2 weeks

package:prod:win64:
  extends: .rust_stable_win64
  only:
    - tags
  stage: package
  script:
    - bash "release/arch/win64/build-win64.sh" "${CI_COMMIT_TAG#v}"
  artifacts:
    paths:
      - work/bin/
    expire_in: 2 weeks

pages:
  extends: .rust_stable_lin64
  stage: publish_doc
  only:
    refs:
      - dev
  except:
    refs:
      - tags
  script:
    - cargo doc
    - mv target/doc public
    - ls public
  artifacts:
    untracked: true
    paths:
      - public

.release_jobs:
  only:
    - tags
  image: rdadev/jinja2:py3.6
  tags:
    - redshift-docker-runner
  script:
    - python3 .gitlab/releaser

prerelease:
  extends: .release_jobs
  stage: prerelease
  variables:
    RELEASE_BIN_DIR: work/bin/
    SOURCE_EXT: '["tar.gz", "zip"]'

release:
  extends: .release_jobs
  stage: release
  allow_failure: false
  when: manual
  variables:
    RELEASE_BIN_DIR: work/bin/
    WIKI_RELEASE: Releases